[logo]: logo.png

<img src="logo.png" alt="logo"/>

## Components

### Common components
* [common-go](https://gitlab.com/ofamily/common-go)
* [common-env](https://gitlab.com/ofamily/common-env)

### Micro-services

#### Infrastructure
* [service-discovery](https://gitlab.com/ofamily/service-discovery)
* [service-gateway](https://gitlab.com/ofamily/service-gateway)

#### Business
* [service-account](https://gitlab.com/ofamily/service-account)
* [service-tree](https://gitlab.com/ofamily/service-tree)
* [service-static](https://gitlab.com/ofamily/service-static)
* [service-webapp](https://gitlab.com/ofamily/service-webapp)

